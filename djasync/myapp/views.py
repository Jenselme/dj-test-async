import json

from django.shortcuts import render
from django.http import JsonResponse, StreamingHttpResponse
from asyncio import sleep as asleep
from time import sleep

from djasync.myapp.models import MyModel


def home(request):
    names = list(MyModel.objects.values_list("name", flat=True))
    return render(request, "myapp/home.html", {"view_name": "home", "names": names})


async def home_async(request):
    names = await alist(MyModel.objects.values_list("name", flat=True))
    response = render(
        request, "myapp/home.html", {"view_name": "home_async", "names": names}
    )
    response[
        "Link"
    ] = "</static/home.css>; as=style; rel=preload, </static/list.css>; as=style; rel=preload"
    return response


def api(request):
    print("hitting api")
    sleep(5)
    print("getting values from api")
    values = list(MyModel.objects.values_list("name", flat=True))
    sleep(5)
    return JsonResponse({"hello": values})


async def aapi(request):
    print("hitting aapi")
    await asleep(5)
    print("getting values from aappi")
    values = await alist(MyModel.objects.values_list("name", flat=True))
    await asleep(5)
    return JsonResponse({"hello": values})


async def alist(values):
    return [val async for val in values]


def stream_sync():
    yield "hello;Bob\n"
    sleep(5)
    yield "hello;Alice\n"
    sleep(5)
    yield "hello;Jane\n"


def stream(request):
    return StreamingHttpResponse(stream_sync(), content_type="text/csv")


async def stream_async():
    yield "hello;Bob\n"
    await asleep(5)
    yield "hello;Alice\n"
    await asleep(5)
    yield "hello;Jane\n"


async def astream(request):
    return StreamingHttpResponse(stream_async(), content_type="text/csv")


async def stream_messages():
    async for model in MyModel.objects.all():
        yield _format_message(model)
        await asleep(5)

    yield """data: {"status": "done"}\n\n"""


def _format_message(model):
    data = json.dumps({"name": model.name, "status": "ongoing"}) + "\n\n"
    return f"data: {data}"


async def stream_messages_view(request):
    return StreamingHttpResponse(
        stream_messages(),
        content_type="text/event-stream",
    )


async def sse_view(request):
    return render(request, "myapp/sse.html")


async def chat_view(request):
    return render(request, "myapp/chat.html")
