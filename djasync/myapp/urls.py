from django.urls import path

from . import views

app_name = "myapp"

urlpatterns = [
    path("sync", views.home),
    path("async", views.home_async),
    path("api", views.api),
    path("aapi", views.aapi),
    path("stream", views.stream),
    path("astream", views.astream),
    path("stream-messages", views.stream_messages_view),
    path("sse", views.sse_view),
    path("chat", views.chat_view),
]
