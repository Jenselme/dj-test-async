from django.urls import path

from djasync.myapp.consumers import ChatConsumer

websockets_urlpatterns = [
    path("ws/chat", ChatConsumer.as_asgi()),
]
