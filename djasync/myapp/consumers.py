from channels.generic.websocket import AsyncJsonWebsocketConsumer


class ChatConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.accept()
        await self.channel_layer.group_add("chat", self.channel_name)

    async def disconnect(self, code):
        await self.channel_layer.group_discard("chat", self.channel_name)

    async def receive_json(self, content, **kwargs):
        await self.channel_layer.group_send(
            "chat", {"type": "chat_message", "message": content}
        )

    async def chat_message(self, event):
        await self.send_json(event["message"])
