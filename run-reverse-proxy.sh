case $1 in
  nginx)
    docker run --rm \
      --name nginx-proxy \
      -p 8080:443 \
      -v $(pwd)/certs:/var/certs \
      -v $(pwd)/static:/var/www/html/static \
      -v $(pwd)/conf/nginx.conf:/etc/nginx/conf.d/default.conf \
      nginx:latest
    ;;
  apache)
    docker run --rm \
      --name apache-proxy \
      -p 8080:443 \
      -v $(pwd)/certs:/var/certs \
      -v $(pwd)/static:/var/www/html/static \
      -v $(pwd)/conf/httpd.conf:/usr/local/apache2/conf/httpd.conf \
      httpd:latest
    ;;
esac
