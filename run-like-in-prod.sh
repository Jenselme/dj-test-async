case $1 in
  daphne)
    daphne djasync.asgi:application -p 8000
    ;;
  daphne-http2)
    daphne djasync.asgi:application -p 8000 -e ssl:8080:privateKey=certs/server.crt:certKey=certs/server.key
    ;;
  gunicorn)
    gunicorn --bind 0.0.0.0:8000 djasync.wsgi --workers 1
    ;;
  uvicorn)
    gunicorn djasync.asgi:application -k uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 --workers 1
    ;;
  hypercorn)
    hypercorn --bind 0.0.0.0:8000 djasync.asgi:application --workers 1
    ;;
  hypercorn-http2)
    hypercorn --bind 0.0.0.0:8080 djasync.asgi:application --workers 1 --certfile certs/server.crt --keyfile certs/server.key
    ;;
  *)
    exec "$1 is not supported to run."
esac
