#!/usr/bin/env bash

case $1 in
  http2)
    echo "Testing https://localhost:8080/async with 1 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 1 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 1 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 1 -m 10  https://localhost:8080/sync
    echo "Testing https://localhost:8080/async with 200 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 200 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 200 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 200 -m 10  https://localhost:8080/sync
    echo "Testing https://localhost:8080/async with 500 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 500 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 500 concurrent connections for 10 000 requests"
    h2load -n 10000 -c 500 -m 10  https://localhost:8080/sync
    ;;
  http1)
    echo "Testing http://localhost:8000/async with 1 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 1 -m 10  http://localhost:8000/async
    echo "Testing http://localhost:8000/sync with 1 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 1 -m 10  http://localhost:8000/sync
    echo "Testing http://localhost:8000/async with 200 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 200 -m 10  http://localhost:8000/async
    echo "Testing http://localhost:8000/sync with 200 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 200 -m 10  http://localhost:8000/sync
    echo "Testing http://localhost:8000/async with 500 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 500 -m 10  http://localhost:8000/async
    echo "Testing http://localhost:8000/sync with 500 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 500 -m 10  http://localhost:8000/sync
    ;;
  http1s)
    echo "Testing https://localhost:8080/async with 1 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 1 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 1 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 1 -m 10  https://localhost:8080/sync
    echo "Testing https://localhost:8080/async with 200 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 200 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 200 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 200 -m 10  https://localhost:8080/sync
    echo "Testing https://localhost:8080/async with 500 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 500 -m 10  https://localhost:8080/async
    echo "Testing https://localhost:8080/sync with 500 concurrent connections for 10 000 requests"
    h2load --h1 -n 10000 -c 500 -m 10  https://localhost:8080/sync
    ;;
  *)
    exec "$1 is not supported to run."
esac
