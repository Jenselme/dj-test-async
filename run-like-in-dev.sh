case $1 in
  daphne)
    python manage.py runserver --nothreading
    ;;
  gunicorn)
    gunicorn --bind 0.0.0.0:8000 djasync.wsgi --workers 1 --reload # --reload-extra-file ./djasync/myapp/templates/myapp/home.html
    ;;
  uvicorn)
    uvicorn djasync.asgi:application --port 8000 --workers 1 --reload
    ;;
  hypercorn)
    hypercorn --bind 0.0.0.0:8000 djasync.asgi:application --workers 1
    ;;
  *)
    exec "$1 is not supported to run."
esac
